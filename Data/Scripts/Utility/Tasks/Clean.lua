local Task=require('Utility.Task')
local Class=require('Class')
local Log=require('Log')
local Character=require('CharacterConstants')
local World = require('World')

local Clean = Class.create(Task)

Clean.CLEAN_TILE_DURATION = 3
Clean.sCleanAnim = 'clean_floor'

function Clean:init(rChar,tPromisedNeeds,rActivityOption)
	print("Starting Cleaning Task")
	Task.init(self, rChar, tPromisedNeeds, rActivityOption)
    self.rTarget = rActivityOption.tData.rTargetObject
    self.duration = self.CLEAN_TILE_DURATION
    self.bInterruptOnPathFailure = true
	self.targetTile={tx=rActivityOption.tBlackboard.tileX,ty=rActivityOption.tBlackboard.tileY}
    assert(rActivityOption.tBlackboard.rChar == rChar)
    local bSuccess = self:_createPathToBuildLoc(self.targetTile.tx,self.targetTile.ty)
	if not bSuccess then
        Print(TT_Warning, 'Character just started build, but cannot find a path to the build location.')
        self:setWait(4)
        self.bStartedWait=true
    end
	
	
	--[[
	for k, v in pairs(World.floorDecals) do
		for k1, v1 in pairs(v) do
			
			if k1 == "tx" then
			
			end
			
		end
	end
	]]--
end


function Clean:onComplete( bSuccess )
	Task.onComplete(self, bSuccess)
	if not bSuccess then
		return
	end
	--clear the floor decal
	self:cleanBlood()
end


function Clean:onUpdate( dt )
	if self:interacting() then
        if self:tickInteraction(dt) then
            return true
        end
	elseif self:tickWalk(dt) then
        if not self:attemptInteractOnObject(self.sCleanAnim, self.rTarget, self.CLEAN_TILE_DURATION) then
            self:interrupt('Failed to reach clean spot.')
        end
	end
end



function Clean:_findCleanSpot(tx,ty)
	local cwx,cwy = self.rChar:getLoc()
    local ctx, cty = g_World._getTileFromWorld(cwx,cwy)
    local wx,wy = g_World._getWorldFromTile(tx,ty)

	-- fallback: just get there. but get out of the way if we're on that tile.
	if g_World.isAdjacentToTile(tx,ty,ctx,cty, true, false) then
		return true
	end

	
	if self:_createCleanerPath(wx,wy,true) then
		return true
	end

    return false
end




function Clean:_createCleanerPath(wxDest,wyDest, bNearest)
    self.tPathParams.bPathToNearest = bNearest
    local wx,wy = self.rChar:getLoc()
    
    local tPath = Pathfinder.getPath(wx, wy, wxDest,wyDest, self.rChar, self.tPathParams)
    if tPath then
        self:setPath(tPath)
        return true
    end
end


function Clean:cleanBlood(tx, ty)
	--clear the floor decal
	World.setFloorDecal(tx,ty,nil)
end


return Clean