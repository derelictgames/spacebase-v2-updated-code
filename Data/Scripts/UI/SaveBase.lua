local Gui = require('UI.Gui')
local DFUtil = require("DFCommon.Util")
local DFInput = require('DFCommon.Input')
local DFFile = require('DFCommon.File')
local UIElement = require('UI.UIElement')
local Renderer = require('Renderer')
local GameRules = require('GameRules')
local GameScreen = require('GameScreen')

local SaveDirectory = require('UI.SaveDirectory')


local sUILayoutFileName = 'UILayouts/SaveBaseLayout'
local platform = "Win"

local m = {}


function cleanPathString(str)
	local finalString = ""
	if platform == "Win" then
		finalString = str:gsub("%/", "\\")
	else
		finalString = str:gsub("%\\", "//")
	end
	
	return finalString
end

function m.create()
    local Ob = DFUtil.createSubclass(UIElement.create())
	local CurrentDir = cleanPathString(tostring(MOAIEnvironment.documentDirectory .. "/Saves"))
	local defaultFileName = "SpacebaseDF9AutoSave.sav" 
	local dirListIndex = 0
	local setSaveFile = ""
	
	Ob.bDoRolloverCheck = true

	
    function Ob:init()
	
        Ob.Parent.init(self)
		
        self:setRenderLayer("UIOverlay")
		
		self:processUIInfo(sUILayoutFileName)
		
		self.uiBG = self:getTemplateElement('BackgroundTop')
		self.uiBG = self:getTemplateElement('BackgroundLeft')
		self.uiBG = self:getTemplateElement('BackgroundRight')
		self.uiBG = self:getTemplateElement('BackgroundBottom')
		
		
		self.logo = self:getTemplateElement('Logo')
		
		if MOAIEnvironment.osBrand == "iOS" or MOAIEnvironment.osBrand == "Android" or MOAIEnvironment.osBrand == "Linux" or MOAIEnvironment.osBrand == "OSX" then
			platform = "Nix"
		else
			platform = 'Win'
		end
		
		--self.uiBG = self:getTemplateElement('Background')
		
		
		
		self.rCurrentPathLabel = self:getTemplateElement('CurrentPathLabel')
		self.rCurrentPathLabel:setString(cleanPathString(CurrentDir))
		self.rCurrentPathLabel.sText = cleanPathString(CurrentDir)
		self.rCurrentPathLabelEditBG = self:getTemplateElement('CurrentPathLabelEditBG')
		self.rCurrentPathEditButton = self:getTemplateElement('CurrentPathEditButton')
		self.rCurrentPathEditButton:addPressedCallback(self.onCurrentPathEditButtonPressed, self)
		
		
		self.rCurrentFileDirectoryList = self:getTemplateElement('CurrentFileDirectoryList')
		self.rCurrentFileDirectoryList:setRenderLayer('UIScrollLayerLeft')
		self.rCurrentFileDirectoryList:setScissorLayer('UIScrollLayerLeft')
    
	
		self.rAlertBox = self:getTemplateElement('AlertBox')
		self.rAlertBox.sText = "" 
		self.rAlertBox:setString("")
		
		
		self.rButtonSave = self:getTemplateElement('SaveButton')
        self.rButtonSave:addPressedCallback(self.preSave, self)
		self.rButtonSaveHotKey = self:getTemplateElement('SaveHotkey')
		
		self.rButtonSaveCancel = self:getTemplateElement('CancelButton')
        self.rButtonSaveCancel:addPressedCallback(self.saveCancel, self)
		self.rButtonSaveCancelHotKey = self:getTemplateElement('CancelHotkey')
	
	end
	

	function Ob:generateDirList()
		local tFiles = MOAIFileSystem.listDirectories(CurrentDir)
		local nFileNumEntries = 0
		
		if table.getn(tFiles) ~= nil then
			nFileNumEntries = table.getn(tFiles)
		end
		
		self:addFileToListEntry(1, "..", "dir")
		
		if nFileNumEntries > 0 then
		
			for k, v in ipairs(tFiles) do
				self:addFileToListEntry(k+1, v, "dir")
			end		
		end
		fileListIndex = nFileNumEntries + 1
		
		local tFiles = MOAIFileSystem.listFiles(CurrentDir)
		local nFileNumEntries = table.getn(tFiles)
		local nFilteredFiles = 0
		
		if nFileNumEntries > 0 then
			
			for k, v in ipairs(tFiles) do
				if string.sub(v, -4) == ".sav" then
					self:addFileToListEntry(k, v, "file")
					nFilteredFiles = nFilteredFiles + 1 
				end
			end		
		end
		
		fileListIndex = fileListIndex + nFilteredFiles + 1
		
		self.rCurrentFileDirectoryList:refresh()

	end
	
	function Ob:addFileToListEntry(id, name, filetype)
		local rNewEntry = self:addFileToList(id, filetype)
		rNewEntry:setName(id, name, filetype, self.onFileClickCallback)
	end
	
	
	function Ob:addFileToList(index, filetype)
		local rNewEntry = SaveDirectory.new(filetype)
		local w,h = rNewEntry:getDims()
		local nYLoc = h * index + 50
		rNewEntry:setLoc(0, nYLoc)
		self:_calcDimsFromElements()
		self.rCurrentFileDirectoryList:addScrollingItem(rNewEntry)
		return rNewEntry
	end
	
	function Ob:onFileClickCallback(id, name, filetype)
		print("id: " .. id .. " name: " .. name .. " filetype: " .. filetype)
		--Activate save Button
		
		if filetype == "dir" then
			CurrentDir = Ob.rCurrentPathLabel.sText
			
			local strSplit = {}
			
			if name == ".." then	
				--print("1. name ..")
				if platform == "Nix" then
					
					local strSplit = {}
					
					for word in string.gmatch(cleanPathString(CurrentDir), '([^//]+)') do
						table.insert(strSplit, word)
					end
					
					local loopMax = table.getn(strSplit)
					CurrentDir = ""
					for k, v in pairs(strSplit) do
						if k < loopMax-1 then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "/" .. v
							end
						end
					end
						
				else
					--print("2. platform win")
					
					for word in string.gmatch(cleanPathString(CurrentDir), '([^\\]+)') do
						table.insert(strSplit, word)
					end
					
					local loopMax = table.getn(strSplit)
					--print("3. loopmax = " .. loopMax)
					--print("3.1 strSplit[loopMax]: " .. strSplit[loopMax])
					--print("3.2 strSplit[loopMax-1]: " .. strSplit[loopMax-1])
					
					CurrentDir = ""
					for k, v in pairs(strSplit) do
						if k < loopMax-1  then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "\\" .. v
							end
						end
					end					
				end
									
				--print("4. CurrentDir: " .. CurrentDir)
				CurrentDir = cleanPathString(CurrentDir)
			else
				if platform == "Nix" then
					if string.sub(CurrentDir, -1) == "//"  then
						CurrentDir = CurrentDir .. name
					else
						CurrentDir = CurrentDir .. "//" .. name
					end
				else
					if string.sub(CurrentDir, -1) == "\\"  then
						CurrentDir = CurrentDir .. name
					else
						CurrentDir = CurrentDir .. "\\" .. name
					end
				end
			end
			
			if MOAIFileSystem.checkPathExists(CurrentDir) then 
				Ob:setElementHidden(Ob.rAlertBox, true) 
				Ob:resetFileList()
				Ob.rCurrentPathLabel:setString(CurrentDir)
				Ob.rCurrentPathLabel.sText = CurrentDir
				Ob.rCurrentPathEditButton:setSelected(false)
				Ob:generateDirList()
				Ob.rCurrentFileDirectoryList:reset()
				
			else
				Ob.rAlertBox:setString("Directory Not Found")
				Ob.rAlertBox.sText = "Directory Not Found"
				Ob:setElementHidden(Ob.rAlertBox, false) 
				Ob:resetFileList()
				Ob.rCurrentFileDirectoryList:refresh()
			end
		elseif filetype == "file" then
			local strSplit = {}
			
			if platform == "Nix" then
				for word in string.gmatch(cleanPathString(CurrentDir), '([^//]+)') do
					table.insert(strSplit, word)
				end
				
				local loopMax = table.getn(strSplit)
				CurrentDir = ""
				if string.sub(strSplit[loopMax], -4, 1) == ".sav" then
					for k, v in pairs(strSplit) do
						if k < loopMax-1 then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "/" .. v
							end
						end
					end
				else
					for k, v in pairs(strSplit) do
						if k < loopMax then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "/" .. v
							end
						end
					end
				end
				CurrentDir = CurrentDir .. "/"
			else
				for word in string.gmatch(cleanPathString(CurrentDir), '([^\\]+)') do
					table.insert(strSplit, word)
				end
				
				local loopMax = table.getn(strSplit)
				
				CurrentDir = ""
				
				if string.sub(tostring(strSplit[loopMax]), -4) == ".sav" then
					--print("2.5 .sav")
					for k, v in pairs(strSplit) do
						if k <= loopMax-1 then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "\\" .. v
							end
						end
					end
				else
					--print("2.5 No .sav")
					for k, v in pairs(strSplit) do
						if k <= loopMax then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "\\" .. v
							end
						end
					end
				end
				CurrentDir = CurrentDir .. "\\"
			end
			CurrentDir = cleanPathString(CurrentDir)
			
			if MOAIFileSystem.checkPathExists(CurrentDir) then 

				CurrentDir = CurrentDir .. name
	
				if MOAIFileSystem.checkFileExists(CurrentDir) then
					Ob:setElementHidden(Ob.rAlertBox, true)
					setSaveFile = name
					Ob.rCurrentPathLabel:setString(CurrentDir)
					Ob.rCurrentPathLabel.sText = CurrentDir
					Ob.rCurrentPathEditButton:setSelected(false)
					Ob.rAlertBox:setString("")
					Ob.rAlertBox.sText = ""
				else
					Ob.rAlertBox:setString("File Not Found")
					Ob.rAlertBox.sText = "File Not Found"
					Ob:setElementHidden(Ob.rAlertBox, false) 
					Ob:resetFileList()
					Ob.rCurrentFileDirectoryList:refresh()
				end
			else
				Ob.rAlertBox:setString("Directory Not Found")
				Ob.rAlertBox.sText = "Directory Not Found"
				Ob:setElementHidden(Ob.rAlertBox, false) 
				Ob:resetFileList()
				Ob.rCurrentFileDirectoryList:refresh()
			end
		else
			Ob.rAlertBox:setString("Directory\\File Not Found")
			Ob.rAlertBox.sText = "Directory\\File Not Found"
			Ob:setElementHidden(Ob.rAlertBox, false) 
			Ob:resetFileList()
			Ob.rCurrentFileDirectoryList:refresh()
		end			
	end
	
	
	function Ob:saveFile()
		
		if g_bGameLoaded == true then
			g_filename = CurrentDir
			GameRules.saveGame() 
			Ob:resetFileList()
			Ob.rCurrentFileDirectoryList:refresh()
			self:setElementHidden(self.rAlertBox, false) 
			self.rAlertBox:setString("Game Saved!")
			self.rAlertBox.sText = "Game Saved!"
			local strSplit = {}
			
			if platform == "Nix" then
				for word in string.gmatch(cleanPathString(CurrentDir), '([^//]+)') do
					table.insert(strSplit, word)
				end
				
				local loopMax = table.getn(strSplit)
				CurrentDir = ""
				if string.sub(strSplit[loopMax], -4, 1) == ".sav" then
					for k, v in pairs(strSplit) do
						if k < loopMax-1 then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "/" .. v
							end
						end
					end
				else
					for k, v in pairs(strSplit) do
						if k < loopMax then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "/" .. v
							end
						end
					end
				end
				CurrentDir = CurrentDir .. "/"
			else
				for word in string.gmatch(cleanPathString(CurrentDir), '([^\\]+)') do
					table.insert(strSplit, word)
				end
				
				local loopMax = table.getn(strSplit)
				
				CurrentDir = ""
				
				if string.sub(tostring(strSplit[loopMax]), -4) == ".sav" then
					--print("2.5 .sav")
					for k, v in pairs(strSplit) do
						if k <= loopMax-1 then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "\\" .. v
							end
						end
					end
				else
					--print("2.5 No .sav")
					for k, v in pairs(strSplit) do
						if k <= loopMax then
							if CurrentDir == "" then
								CurrentDir = v
							else
								CurrentDir = CurrentDir .. "\\" .. v
							end
						end
					end
				end
				CurrentDir = CurrentDir .. "\\"
			end
			
			CurrentDir = cleanPathString(CurrentDir)
			
			Ob:resetFileList()
			Ob:generateDirList()
			
		else
			self:setElementHidden(self.rAlertBox, false) 
			self.rAlertBox:setString("Game Not Loaded")
			self.rAlertBox.sText = "Game Not Loaded"
		end
		
	end
	
	function Ob:removeFileEntry(nIndex)
		self.rCurrentFileDirectoryList:removeScrollingItem(self.rCurrentFileDirectoryList.tItems[nIndex])
	end
	
	function Ob:resetFileList()
		for iLoop = fileListIndex, 1, -1 do
			self.rCurrentFileDirectoryList:removeScrollingItem(self.rCurrentFileDirectoryList.tItems[iLoop])
		end
		
	end
	

	function Ob:onCurrentPathEditButtonPressed(rButton, eventType)
		if eventType == DFInput.TOUCH_UP and not GameScreen.inTextEntry() then
			g_inLoadOrSave = true
			GameScreen.beginTextEntry(self.rCurrentPathLabel, self, self.confirmTextEntry, self.cancelTextEntry)
			self.rCurrentPathEditButton:setSelected(true)
			self.rCurrentFileDirectoryList:refresh()
		end
	end
	
	function Ob:preSave()
		Ob:confirmTextEntry(CurrentDir)
	end
	

	function Ob:confirmTextEntry(text)
		--print("1. text: " .. text)
		if text then
			--print("2. string.sub(text, -4): " .. string.sub(text, -4) )
			
			if string.sub(text, -4) ~= ".sav" then
				CurrentDir = cleanPathString(text)
				
				if MOAIFileSystem.checkPathExists(CurrentDir) then 
					self:setElementHidden(self.rAlertBox, true) 
					self:resetFileList()
					self.rCurrentPathLabel:setString(CurrentDir)
					self.rCurrentPathLabel.sText = CurrentDir
					self.rCurrentPathEditButton:setSelected(false)
					self:generateDirList()
					self.rCurrentFileDirectoryList:reset()
					
				else
					self.rAlertBox:setString("Directory\\File Not Found")
					self.rAlertBox.sText = "Directory\\File Not Found"
					self:setElementHidden(self.rAlertBox, false) 
					self:resetFileList()
					self.rCurrentFileDirectoryList:refresh()
				end
			else
				print("CurrentDir: " .. text)
				CurrentDir = text
				self.rAlertBox:setString("")
				self.rAlertBox.sText = ""
				self:setElementHidden(self.rAlertBox, true) 
				Ob:saveFile()
			end
		end
	end
	
	function Ob:cancelTextEntry(text)
		self.rCurrentPathEditButton:setSelected(false)
		g_inLoadOrSave = false
	end
	

	function Ob:saveCancel()
		self:resetFileList()
		CurrentDir = cleanPathString(tostring(MOAIEnvironment.documentDirectory .. "/Saves"))
		self.rCurrentPathLabel:setString(CurrentDir)
		self.rCurrentPathLabel.sText = CurrentDir
		self.rAlertBox.sText = ""
		self.rAlertBox:setString("")
		g_GuiManager.showStartMenu(true)
	end
	
    function Ob:_updateBackground()
        --self.uiBG:setScl(Renderer.getViewport().sizeX*2,Renderer.getViewport().sizeY*2)
        --self.uiBG:setLoc(-Renderer.getViewport().sizeX*.5,Renderer.getViewport().sizeY*.5)
	end
	
	function Ob:show(basePri)
		Ob.Parent.show(self, basePri)
		
	    g_GuiManager.statusBar:hide()
		g_GuiManager.tutorialText:hide()
        g_GuiManager.hintPane:hide()
        g_GuiManager.alertPane:hide()
		g_GuiManager.newSideBar:hide()
		
        GameRules.timePause()
        self:onResize()
		
		self:generateDirList()
		self.rCurrentFileDirectoryList:reset()
		return nPri
	end
	

    function Ob:onTick(dt)
        Ob.Parent.onTick(self, dt)
    end

    function Ob:playWarbleEffect(bFullscreen)
        if bFullscreen then
            local uiX,uiY,uiW,uiH = Renderer.getUIViewportRect()            
            g_GuiManager.createEffectMaskBox(0, 0, uiW, uiH, 0.3)
        else
            g_GuiManager.createEffectMaskBox(0, 0, 500, 1444, 0.3, 0.3)
        end
    end    
	
    function Ob:onFinger(touch, x, y, props)
		Ob.Parent.onFinger(self, touch, x, y, props)
		if self.rCurrentFileDirectoryList:onFinger(touch, x, y, props) then
            bHandled = true
        end
        if self.rCurrentFileDirectoryList:isInsideScrollPane(x, y) then
                   
        end
    end
	
    function Ob:inside(wx, wy)
		self.rCurrentFileDirectoryList:inside(wx, wy)
		return Ob.Parent.inside(self, wx, wy)
    end
	
	function Ob:onKeyboard(key, bDown)
        -- capture all keyboard input
        if bDown and key == 27 then -- esc
			self:saveCancel()
        end
		
		if bDown and key == 67 then -- C
			self:saveCancel()
		end
		
		if bDown and key == 99 then -- c
			self:saveCancel()
		end
		
		if bDown and key == 83 then -- S
			self:preSave()
		end
		
		if bDown and key == 115 then -- s
			self:preSave()
		end
		
        return true
    end
	
	function Ob:onResize()
        Ob.Parent.onResize(self)
        self:_updateBackground()
    end
	
	function Ob:onBackButtonPressed(rButton, eventType)
        if eventType == DFInput.TOUCH_UP then
			menuManager.closeMenu()
			SoundManager.playSfx('degauss')
        end
    end
   
    return Ob
end

function m.new(...)
    local Ob = m.create()
    Ob:init(...)

    return Ob
end

return m
