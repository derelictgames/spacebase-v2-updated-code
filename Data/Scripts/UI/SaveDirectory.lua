local Gui = require('UI.Gui')
local DFUtil = require("DFCommon.Util")
local DFInput = require('DFCommon.Input')
local UIElement = require('UI.UIElement')
local Renderer = require('Renderer')
local SoundManager = require('SoundManager')

local m = {}


local sUILayoutFileName = 'UILayouts/SaveDirectoryLayout'


function cleanPathString(str)
	finalString = str:gsub("%/", "\\")
	return finalString
end


function m.create(_filetype)
	local Ob = DFUtil.createSubclass(UIElement.create())
	local id
	local name
	local filetype
	
	function Ob:init(_filetype)

		self:setRenderLayer('UIScrollLayerLeft')
		
        Ob.Parent.init(self)
		self:processUIInfo(sUILayoutFileName)

		self.rFolderIcon = self:getTemplateElement('FolderIcon')
		self.rNameLabel = self:getTemplateElement('NameLabel')
		self.rNameButton = self:getTemplateElement('NameButton')
		self.rNameButton:addPressedCallback(self.onButtonClicked, self)
		
		if _filetype == "dir" then
			self:setElementHidden(self.rFolderIcon, false) 
		end
		
		self:_calcDimsFromElements()
		
	end
	
	function Ob:setName(_id, _name, _filetype, callback)
		id = _id
		name = _name
		filetype = _filetype
		self.rNameLabel:setString(name)
		self.callback = callback
	end
	
	function Ob:onButtonClicked(rButton, eventType)
		if eventType == DFInput.TOUCH_UP then
			self:callback(id, name, filetype)
			SoundManager.playSfx('accept')
		end
	end
	
	return Ob
end

function m.new(...)
    local Ob = m.create()
    Ob:init(...)

    return Ob
end

return m