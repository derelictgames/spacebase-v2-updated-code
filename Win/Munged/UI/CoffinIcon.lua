-- This file is for use with Moai SDK
-- 
-- This file is automatically generated with TexturePacker - https://www.codeandweb.com/texturepacker
-- $TexturePacker:SmartUpdate:7b4ef2da37a244e5e9cec762146b304c:c6583dc5a496c97d88f7ad225a4b5996:d8d0e4dfd68bcafbc63e9742c419528b$
-- 

return {
	texture = 'CoffinIcon.png',
	frames = {
			{
				name = "CoffinIcon",
				spriteColorRect = { x = 3, y = 0, width = 26, height = 32 },
				uvRect = { u0 = 0.0357143, v0 = 0.0294118, u1 = 0.964286, v1 = 0.970588 },
				spriteSourceSize = { width = 32, height = 32 },
				spriteTrimmed = true,
				textureRotated = false
			},
		}
	}

